package com.example.third_home_task

import android.app.Application
import com.example.third_home_task.database.AppDatabase

class ContactsApplication : Application() {
    val database: AppDatabase by lazy { AppDatabase.getDatabase(this) }
}