package com.example.third_home_task

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.third_home_task.database.Contacts
import com.example.third_home_task.databinding.ContactListItemBinding

class ItemListAdapter(private val onContactClicked: (Contacts) -> Unit) :
    ListAdapter<Contacts, ItemListAdapter.ItemViewHolder>(DiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ContactListItemBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onContactClicked(current)
        }
        holder.bind(current)
    }

    class ItemViewHolder(private var binding: ContactListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Contacts) {
            binding.tvNumberItem.text = item.phoneNumber
        }
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Contacts>() {
            override fun areItemsTheSame(oldItem: Contacts, newItem: Contacts): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Contacts, newItem: Contacts): Boolean {
                return oldItem.phoneNumber == newItem.phoneNumber
            }
        }
    }
}