package com.example.third_home_task

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.third_home_task.database.Contacts
import com.example.third_home_task.databinding.FragmentMainBinding
import com.example.third_home_task.viewmodels.ContactViewModel
import com.example.third_home_task.viewmodels.ContactViewModelFactory

val REQUEST_CODE = 1

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding

    //    private var launcher: ActivityResultLauncher<Intent>? = null
    private val viewModel: ContactViewModel by activityViewModels {
        ContactViewModelFactory(
            (activity?.application as ContactsApplication).database
                .contactsDao()
        )
    }
    lateinit var contacts: Contacts
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater)
        return binding.root
    }

    //    @SuppressLint("Recycle")
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        launcher =
//            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
//                    result: ActivityResult ->
//                if (result.resultCode == RESULT_OK) {
//                    val contactUri = result.data
//                    val projectionNumber = ContactsContract.CommonDataKinds.Phone.NUMBER
//                    val projectionName = ContactsContract.CommonDataKinds.Nickname.DISPLAY_NAME
//                    val projectionEmail = ContactsContract.CommonDataKinds.Email.DATA
//                    val contentResolver = activity?.contentResolver
//                    val cursor = contentResolver?.query(contactUri,null,null,null,null)
//                    cursor?.moveToFirst()
//                    val columnNumber = cursor?.getColumnIndex(projectionNumber)
//                    val hisNumber = cursor?.getString(columnNumber!!)
//                    Log.d("TAG", "contact = $hisNumber")
//
//                    val columnName = cursor?.getColumnIndex(projectionName)
//                    val hisName = cursor?.getString(columnName!!)
//                    Log.d("TAG", "contact = $hisName")
//
//                    val columnEmail = cursor?.getColumnIndex(projectionEmail)
//                    val hisEmail = cursor?.getString(columnEmail!!)
//                    Log.d("TAG", "contact = $hisEmail")
//
//                }
//            }
//    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.bSelectContact.setOnClickListener {
            val permissionStatus =
                context?.let { it1 ->
                    ContextCompat.checkSelfPermission(
                        it1,
                        Manifest.permission.READ_CONTACTS
                    )
                }
            if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, REQUEST_CODE)
            } else {
                ActivityCompat.requestPermissions(
                    context as Activity,
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    PackageManager.PERMISSION_GRANTED
                )
                Toast.makeText(
                    context,
                    "Требуется установить разрешение на доступ к контактам",
                    Toast.LENGTH_SHORT
                ).show()
                if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
                    val pickContactIntent =
                        Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                    pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                    startActivityForResult(pickContactIntent, REQUEST_CODE)
                }
            }
        }
        binding.bShowContacts.setOnClickListener {
//            viewModel.allNumbers.observe(this.viewLifecycleOwner) { items ->
//                items.let {
//                    listNumber = it
//                }
//            }
            showDialog(view)
        }
        binding.bShowFromSP.setOnClickListener {
//            val itemListDialogFragment = ItemListDialogFragment()
//            val number = itemListDialogFragment.getNumberSP()
            Toast.makeText(context, "test 11111111", Toast.LENGTH_SHORT).show()
        }
//        binding.bSelectContact.setOnClickListener {
//            val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
//            pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
//            launcher?.launch(pickContactIntent)
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            val contactUri = data.data
            val projectionNumber = ContactsContract.CommonDataKinds.Phone.NUMBER
            val projectionName = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
//            val projectionName = ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME
//            val projectionSurname = ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME
            val contentResolver = activity?.contentResolver
            val cursor =
                contactUri?.let { it1 -> contentResolver?.query(it1, null, null, null, null) }
            cursor?.moveToFirst()
            val columnNumber = cursor?.getColumnIndex(projectionNumber)
            val hisNumber = columnNumber?.let { cursor.getString(it) }
            val columnName = cursor?.getColumnIndex(projectionName)
            val hisName = columnName?.let { cursor.getString(it) }
            val fullName: List<String>? = hisName?.split(" ")
            var nameContact = ""
            var surnameContact = ""
            if (fullName?.size == 2 && fullName.isNotEmpty()) {
                nameContact = fullName[0]
                surnameContact = fullName[1]
            } else {
                nameContact = fullName?.get(0).toString()
                surnameContact = "no info"
            }
            val emailUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI
            val projectionEmail = ContactsContract.CommonDataKinds.Email.ADDRESS
            val cursor2 =
                emailUri?.let { it1 -> contentResolver?.query(it1, null, null, null, null) }
            cursor2?.moveToNext()
            val columnEmail = cursor2?.getColumnIndex(projectionEmail)
            val emailContact = columnEmail?.let { cursor2.getString(it) }
            addNewContact(
                hisNumber.toString(),
                nameContact,
                surnameContact,
                emailContact.toString()
            )
            Toast.makeText(context, "Сохранение успешно", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addNewContact(number: String, name: String, surname: String, email: String) {
        viewModel.addNewContact(number, name, surname, email)
    }

    fun showDialog(view: View) {
        val dialogWindow = ItemListDialogFragment()
        activity?.let { dialogWindow.show(it.supportFragmentManager, "custom") }
    }

    companion object {
        @JvmStatic
        fun newInstance() = MainFragment()
    }
}