package com.example.third_home_task.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(contacts: Contacts)

    @Query("SELECT phone_number FROM contacts")
    fun getPhoneNumbers(): Flow<List<String>>

    @Query("SELECT * FROM contacts WHERE phone_number = :phoneNumber")
    fun getInfoContact(phoneNumber: String): Flow<Contacts>

    @Query("SELECT * from contacts ORDER BY name ASC")
    fun getContacts(): Flow<List<Contacts>>
}