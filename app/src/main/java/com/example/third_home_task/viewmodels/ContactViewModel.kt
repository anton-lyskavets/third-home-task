package com.example.third_home_task.viewmodels

import androidx.lifecycle.*
import com.example.third_home_task.database.Contacts
import com.example.third_home_task.database.ContactsDao
import kotlinx.coroutines.launch
import androidx.lifecycle.asLiveData

class ContactViewModel(private val contactsDao: ContactsDao) : ViewModel() {
    val allContacts: LiveData<List<Contacts>> = contactsDao.getContacts().asLiveData()
    val allNumbers: LiveData<List<String>> = contactsDao.getPhoneNumbers().asLiveData()
    private fun insertContact(contact: Contacts) {
        viewModelScope.launch {
            contactsDao.insert(contact)
        }
    }

    private fun getNewContactEntry(
        contactPhoneNumber: String,
        contactName: String,
        contactSurname: String,
        contactEmail: String
    ): Contacts {
        return Contacts(
            phoneNumber = contactPhoneNumber,
            name = contactName,
            surname = contactSurname,
            email = contactEmail
        )
    }

    fun addNewContact(
        contactPhoneNumber: String,
        contactName: String,
        contactSurname: String,
        contactEmail: String
    ) {
        val newContact = getNewContactEntry(
            contactPhoneNumber,
            contactName,
            contactSurname,
            contactEmail
        )
        insertContact(newContact)
    }
}

class ContactViewModelFactory(
    private val contactsDao: ContactsDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ContactViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ContactViewModel(contactsDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
