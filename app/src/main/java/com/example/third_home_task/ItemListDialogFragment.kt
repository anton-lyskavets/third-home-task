package com.example.third_home_task

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.example.third_home_task.databinding.FragmentCustomDialogBinding
import com.example.third_home_task.viewmodels.ContactViewModel
import com.example.third_home_task.viewmodels.ContactViewModelFactory

class ItemListDialogFragment : DialogFragment() {
    lateinit var sharedPref: SharedPreferences
    private val saveKey = "save_key"
    private lateinit var binding: FragmentCustomDialogBinding
    var arrayTest = arrayOf("11111", "22222", "33333")
    private val viewModel: ContactViewModel by activityViewModels {
        ContactViewModelFactory(
            (activity?.application as ContactsApplication).database.contactsDao()
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle("Номера контактов:")
                .setItems(arrayTest,
                    DialogInterface.OnClickListener { dialog, which ->
                        initSP()
                        saveNumberSP(arrayTest[which])
                        Toast.makeText(
                            context,
                            "testName, testSurname, testEmail",
                            Toast.LENGTH_SHORT
                        ).show()
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCustomDialogBinding.inflate(inflater)
        return binding.root
    }

    fun initSP() {
        sharedPref = (activity?.getSharedPreferences("Контакты", MODE_PRIVATE)
            ?: "no info") as SharedPreferences
    }

    @SuppressLint("CommitPrefEdits")
    fun saveNumberSP(number: String) {
        val edit: SharedPreferences.Editor = sharedPref.edit()
        edit.putString(saveKey, number)
        edit.apply()
    }

    fun getNumberSP() {
        sharedPref.getString(saveKey, "No number in SP!")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ItemListAdapter {
        }
        viewModel.allContacts.observe(this.viewLifecycleOwner) { items ->
            items.let {
                for (i in it) {
                    adapter.submitList(it)
                }
            }
        }
//        viewModel.allNumbers.observe(this.viewLifecycleOwner) { items ->
//            items.let {
//                arrayTest = it.toTypedArray()
//            }
//        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ItemListDialogFragment()
    }
}